/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.tarea1semana2;

/**
 *
 * @author fabri
 */
public class Aranae extends Insecto {
    
    public String venenosa, tipoCaza;
    
    public Aranae(String venenosa, String tipoCaza, String pNombre, String pPatas, 
            String pClase, String pOrden, String pAlimentacion){
        super(pNombre, pPatas, pClase, pOrden, pAlimentacion);
        this.venenosa = venenosa;
        this.tipoCaza = tipoCaza;
    }
    public void atributos(){
        System.out.println("Venenosa: "+this.venenosa+"\nTipo de cazeria: " + this.tipoCaza+"\nNombre: " + this.nombre 
                + "\nCantidad de patas: " + this.cantidadPatas + 
                "\nClase: " + this.clase + "\nOrden: " + this.orden + 
                "\nAlimentacion: " + this.alimentacion);
    }
    public void clasificacion(){
        System.out.println("La clasificacion es Araneae");
    }
    public void social(){
        System.out.println("Especie solitaria");
    }
    
}
