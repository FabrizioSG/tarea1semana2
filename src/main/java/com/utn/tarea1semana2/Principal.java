/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.tarea1semana2;

/**
 *
 * @author fabri
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Insecto insecto1 = new Insecto("bupréstidos", "4", "Insecta", "Coleoptera", "Herbivoro");
        insecto1.atributos();
        insecto1.clasificacion();
        insecto1.social();
        
        Isoptera isoptera1 = new Isoptera("Madera humeda","Soldado","Termita","6", "Insecta","Blattodea","Herbivoro");
        isoptera1.atributos();
        isoptera1.clasificacion();
        isoptera1.social();
        
        Aranae aranae1 = new Aranae("No","Red","Latrodectus","8","Arachnida","Araneae","Carnivoro");
        aranae1.atributos();
        aranae1.clasificacion();
        aranae1.social();
        
        
            
        }
    public void clasificacion(Insecto insecto){
        insecto.clasificacion();
        
    }
    public void social(Insecto insecto){
        insecto.social();
    }
    
}
