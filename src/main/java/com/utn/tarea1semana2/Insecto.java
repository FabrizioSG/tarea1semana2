/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.tarea1semana2;

/**
 *
 * @author fabri
 */
public class Insecto {
    public String nombre, cantidadPatas, clase, orden, alimentacion;
    
    public Insecto(String pNombre, String pPatas, String pClase, String pOrden, String pAlimentacion){
        this.nombre = pNombre;
        this.cantidadPatas = pPatas;
        this.clase = pClase;
        this.orden = pOrden;
        this.alimentacion = pAlimentacion;
    }
    
    public void atributos(){
        System.out.println("Nombre: " + this.nombre + "\nCantidad de patas: " + this.cantidadPatas + 
                "\nClase: " + this.clase + "\nOrden: " + this.orden + 
                "\nAlimentacion: " + this.alimentacion);
    }
    
    public void clasificacion(){
        System.out.println("clasificacion sin especificar");
    }
    
    public void social(){
        System.out.println("Sociabildiad sin especificar");
    }
    
}
