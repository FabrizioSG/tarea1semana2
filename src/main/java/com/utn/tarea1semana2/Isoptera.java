/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utn.tarea1semana2;

/**
 *
 * @author fabri
 */
public class Isoptera extends Insecto {
    
    public String tipo, casta;
    
    public Isoptera(String pTipo, String pCasta, String pNombre, String pPatas, String pClase, 
            String pOrden, String pAlimentacion){
        super(pNombre, pPatas, pClase, pOrden, pAlimentacion);
        this.tipo = pTipo;
        this.casta = pCasta;
    }
    public void atributos(){
        System.out.println("Tipo de termita: "+this.tipo+"\nCasta: " + this.casta+"\nNombre: " + this.nombre 
                + "\nCantidad de patas: " + this.cantidadPatas + 
                "\nClase: " + this.clase + "\nOrden: " + this.orden + 
                "\nAlimentacion: " + this.alimentacion);
        
    }
    public void clasificacion(){
        System.out.println("La clasificacio es Isoptera");
    }
    public void social(){
        System.out.println("Especie sociable");
    }
}
